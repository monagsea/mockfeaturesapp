package com.example.feature_drinks.data.repo

import com.example.di.provideDatabase
import com.example.feature_drinks.data.remote.DrinksService
import com.example.feature_drinks.data.remote.response.CategoryDrinksDTO
import com.example.feature_drinks.data.remote.response.DrinkDetailDTO
import com.example.feature_drinks.di.provideRetrofit
import com.example.local.database.MainDatabase
import com.example.local.drinksLocal.entity.Category
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val remoteModule = module {
    single { DrinksRepo(provideRetrofit(get()), provideDatabase(androidContext())) }
}

class DrinksRepo(private val service: DrinksService, database: MainDatabase) {

    private val categoryDao = database.categoryDao()

    suspend fun getDrinkCategories(): List<Category> = withContext(Dispatchers.IO) {
        return@withContext categoryDao.getCategories().ifEmpty {
            val categoryList: List<Category> = service.getCategories().categoryItems
                .map {
                    Category(
                        category_name = it.strCategory
                    )
                }
            categoryDao.insertCategory(categoryList)
            return@ifEmpty categoryList
        }
    }

    suspend fun getDrinksByCategory(category: String): CategoryDrinksDTO =
        withContext(Dispatchers.IO) {
            service.getDrinksByCategory(category)
        }

    suspend fun getDrinkDetailsById(id: String): DrinkDetailDTO = withContext(Dispatchers.IO) {
        service.getDrinkDetailsById(id)
    }


}