package com.example.feature_drinks.data.remote.response

import com.google.gson.annotations.SerializedName

data class CategoryListDTO(
    @SerializedName("drinks")
    val categoryItems: List<CategoryItem>
) {
    data class CategoryItem(
        val strCategory: String
    )
}