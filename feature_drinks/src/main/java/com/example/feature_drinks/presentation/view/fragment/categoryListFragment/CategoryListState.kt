package com.example.feature_drinks.presentation.view.fragment.categoryListFragment

import com.example.local.drinksLocal.entity.Category


data class CategoryListState(
    val categories: List<Category> = emptyList()
)
