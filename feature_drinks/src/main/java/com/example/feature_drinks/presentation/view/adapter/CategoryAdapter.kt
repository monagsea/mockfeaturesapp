package com.example.feature_drinks.presentation.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.feature_drinks.databinding.ItemCategoryBinding
import com.example.feature_drinks.presentation.view.adapter.CategoryAdapter.CategoryAdapterViewHolder.Companion.getInstance
import com.example.feature_drinks.presentation.view.fragment.categoryListFragment.CategoryListFragmentDirections
import com.example.local.drinksLocal.entity.Category


class CategoryAdapter : RecyclerView.Adapter<CategoryAdapter.CategoryAdapterViewHolder>() {

    private var categories = listOf<Category>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoryAdapterViewHolder {
        return getInstance(parent)
    }

    override fun onBindViewHolder(holder: CategoryAdapterViewHolder, position: Int) {
        holder.bindCategory(categories[position])
    }

    override fun getItemCount(): Int {
        return categories.size
    }

    fun addCategories(categories: List<Category>) {
        this.categories = categories
        notifyDataSetChanged()
    }

    class CategoryAdapterViewHolder(private val binding: ItemCategoryBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bindCategory(category: Category) = with(binding) {
            tvItemCategory.text = category.category_name
            tvItemCategory.setOnClickListener {
                it.findNavController().navigate(
                    CategoryListFragmentDirections.actionCategoryListFragmentToDrinkListFragment(
                        category.category_name
                    )
                )
            }
        }

        companion object {
            fun getInstance(parent: ViewGroup) = ItemCategoryBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            ).let { CategoryAdapterViewHolder(it) }
        }

    }

}