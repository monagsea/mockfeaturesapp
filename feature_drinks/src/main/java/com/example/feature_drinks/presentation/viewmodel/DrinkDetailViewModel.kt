package com.example.feature_drinks.presentation.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.feature_drinks.data.remote.response.DrinkDetailDTO
import com.example.feature_drinks.domain.GetDrinkDetailsUseCase
import com.example.feature_drinks.presentation.view.fragment.drinkDetailsFragment.DrinkDetailsState
import kotlinx.coroutines.launch


class DrinkDetailViewModel(
    private val getDrinkDetailsUseCase: GetDrinkDetailsUseCase
) : ViewModel() {

    private var _drinkDetailState = MutableLiveData(DrinkDetailsState())
    val drinkDetailsState: LiveData<DrinkDetailsState> get() = _drinkDetailState

    private var _drinkDetails = MutableLiveData<DrinkDetailDTO>()
    val drinkDetails: LiveData<DrinkDetailDTO> get() = _drinkDetails

    fun getDrinkDetails(id: String) {
        viewModelScope.launch {
            _drinkDetailState.value = DrinkDetailsState(getDrinkDetailsUseCase.invoke(id).drinks)
            _drinkDetails.value = getDrinkDetailsUseCase.invoke(id)
        }
    }
}