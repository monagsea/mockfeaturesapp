package com.example.feature_drinks.presentation.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.feature_drinks.data.remote.response.CategoryDrinksDTO
import com.example.feature_drinks.databinding.ItemDrinksBinding
import com.example.feature_drinks.presentation.view.fragment.drinkListFragment.DrinkListFragmentDirections
import com.squareup.picasso.Picasso

class DrinksAdapter : RecyclerView.Adapter<DrinksAdapter.DrinksAdapterViewHolder>() {

    private var drinks = listOf<CategoryDrinksDTO.Drink>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DrinksAdapterViewHolder {
        return DrinksAdapterViewHolder.getInstance(parent)
    }

    override fun onBindViewHolder(holder: DrinksAdapterViewHolder, position: Int) {
        holder.bindDrinks(drinks[position])
    }

    override fun getItemCount(): Int {
        return drinks.size
    }

    fun addDrinks(drinksDTO: CategoryDrinksDTO) {
        drinks = drinksDTO.drinks
        notifyDataSetChanged()
    }

    class DrinksAdapterViewHolder(private val binding: ItemDrinksBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bindDrinks(drinks: CategoryDrinksDTO.Drink) = with(binding) {
            tvDrinkItem.text = drinks.strDrink
            Picasso.get().load(drinks.strDrinkThumb).into(ivDrinkItem)
            root.setOnClickListener {
                root.findNavController().navigate(
                    DrinkListFragmentDirections.actionDrinkListFragmentToDrinkDetailsFragment(drinks.idDrink)
                )
            }
        }

        companion object {
            fun getInstance(parent: ViewGroup) = ItemDrinksBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            ).let { DrinksAdapterViewHolder(it) }
        }
    }
}