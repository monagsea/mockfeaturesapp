package com.example.feature_drinks.presentation.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.feature_drinks.data.remote.response.DrinkDetailDTO
import com.example.feature_drinks.databinding.ItemDetailsBinding
import com.squareup.picasso.Picasso

class DrinkDetailsAdapter : RecyclerView.Adapter<DrinkDetailsAdapter.DrinkDetailsViewHolder>() {

    var details = listOf<DrinkDetailDTO.Drink>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DrinkDetailsViewHolder {
        return DrinkDetailsViewHolder.getInstance(parent)
    }

    override fun onBindViewHolder(holder: DrinkDetailsViewHolder, position: Int) {
        holder.bindDetails(details[position])
    }

    override fun getItemCount(): Int {
        return details.size
    }

    fun addDetails(details: List<DrinkDetailDTO.Drink>) {
        this.details = details
        notifyDataSetChanged()
    }

    class DrinkDetailsViewHolder(private val binding: ItemDetailsBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bindDetails(details: DrinkDetailDTO.Drink) {
            with(binding) {
                tvDetailDrinkName.text = details.strDrink
                Picasso.get().load(details.strDrinkThumb).into(ivDrinkDetail)
                simpleCheckedTextView1.text = details.strIngredient1
                simpleCheckedTextView2.text = details.strIngredient2
                simpleCheckedTextView3.text = details.strIngredient3
            }
        }

        companion object {
            fun getInstance(parent: ViewGroup) = ItemDetailsBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            ).let {
                DrinkDetailsViewHolder(it)
            }
        }
    }
}