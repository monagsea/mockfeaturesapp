package com.example.feature_drinks.presentation.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.feature_drinks.data.remote.response.CategoryDrinksDTO
import com.example.feature_drinks.domain.GetDrinkListByIdUseCase
import com.example.feature_drinks.presentation.view.fragment.drinkListFragment.DrinkListState
import kotlinx.coroutines.launch


class DrinkListViewModel(
    private val getDrinkListByIdUseCase: GetDrinkListByIdUseCase
) : ViewModel() {

    private var _drinkListState = MutableLiveData(DrinkListState())
    val drinkListState: LiveData<DrinkListState> get() = _drinkListState

    private var _drinkList = MutableLiveData<CategoryDrinksDTO>()
    val drinkList: LiveData<CategoryDrinksDTO> get() = _drinkList

    fun getDrinksById(category: String) {
        viewModelScope.launch {
            _drinkListState.value =
                DrinkListState(drinks = getDrinkListByIdUseCase.invoke(category).drinks)
            _drinkList.value = getDrinkListByIdUseCase.invoke(category)
        }
    }

}