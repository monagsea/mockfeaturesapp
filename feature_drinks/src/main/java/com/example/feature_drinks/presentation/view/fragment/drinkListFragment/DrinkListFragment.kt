package com.example.feature_drinks.presentation.view.fragment.drinkListFragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.feature_drinks.databinding.FragmentDrinkListBinding
import com.example.feature_drinks.presentation.view.adapter.DrinksAdapter
import com.example.feature_drinks.presentation.viewmodel.DrinkListViewModel
import org.koin.android.ext.android.get


class DrinkListFragment : Fragment() {

    private var _binding: FragmentDrinkListBinding? = null
    private val binding: FragmentDrinkListBinding get() = _binding!!
    private val drinksAdapter by lazy { DrinksAdapter() }
    private val drinkListViewModel = get<DrinkListViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentDrinkListBinding.inflate(
        inflater, container, false
    ).also { _binding = it }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val category = arguments?.getString("category")

        with(binding) {
            with(rvCategories) {
                layoutManager = LinearLayoutManager(context)
                adapter = drinksAdapter.apply {
                    with(drinkListViewModel) {
                        getDrinksById(category!!)
                        drinkList.observe(viewLifecycleOwner) {
                            addDrinks(it)
                        }
                    }
                }
            }
        }
    }

}