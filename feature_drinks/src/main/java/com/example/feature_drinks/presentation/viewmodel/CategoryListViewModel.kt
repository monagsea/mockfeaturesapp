package com.example.feature_drinks.presentation.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.feature_drinks.domain.GetDrinkCategoriesUseCase
import com.example.feature_drinks.presentation.view.fragment.categoryListFragment.CategoryListState
import com.example.local.drinksLocal.entity.Category
import kotlinx.coroutines.launch


class CategoryListViewModel(
    private val getDrinkCategoriesUseCase: GetDrinkCategoriesUseCase
) : ViewModel() {

    private val _categoryListState = MutableLiveData(CategoryListState())
    val categoryListState: LiveData<CategoryListState> get() = _categoryListState

    private val _categoryList = MutableLiveData<List<Category>>()
    val categoryList: LiveData<List<Category>> get() = _categoryList

    fun browseCategories() {
        viewModelScope.launch {
            _categoryListState.value =
                CategoryListState(categories = getDrinkCategoriesUseCase.invoke())
            _categoryList.value = getDrinkCategoriesUseCase.invoke()
        }
    }

}