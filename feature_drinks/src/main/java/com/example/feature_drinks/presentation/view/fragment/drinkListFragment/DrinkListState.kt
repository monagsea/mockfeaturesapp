package com.example.feature_drinks.presentation.view.fragment.drinkListFragment

import com.example.feature_drinks.data.remote.response.CategoryDrinksDTO

data class DrinkListState(
    val drinks: List<CategoryDrinksDTO.Drink> = emptyList()
)
