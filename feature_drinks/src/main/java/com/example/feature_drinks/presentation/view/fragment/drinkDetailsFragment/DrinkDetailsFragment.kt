package com.example.feature_drinks.presentation.view.fragment.drinkDetailsFragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.feature_drinks.databinding.FragmentDrinkDetailsBinding
import com.example.feature_drinks.presentation.view.adapter.DrinkDetailsAdapter
import com.example.feature_drinks.presentation.viewmodel.DrinkDetailViewModel
import org.koin.android.ext.android.get

class DrinkDetailsFragment : Fragment() {

    private var _binding: FragmentDrinkDetailsBinding? = null
    private val binding: FragmentDrinkDetailsBinding get() = _binding!!
    private val drinkDetailsViewModel = get<DrinkDetailViewModel>()
    private val detailsAdapter by lazy { DrinkDetailsAdapter() }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentDrinkDetailsBinding.inflate(
        inflater, container, false
    ).also { _binding = it }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val id = arguments?.getString("id")

        Log.i("TEST2", "onViewCreated: $id")

        drinkDetailsViewModel.getDrinkDetails(id!!)
        binding.rvDetails.layoutManager = LinearLayoutManager(context)
        binding.rvDetails.adapter = detailsAdapter.apply {
            drinkDetailsViewModel.drinkDetails.observe(viewLifecycleOwner) {
                addDetails(it.drinks)
            }
        }
    }

}