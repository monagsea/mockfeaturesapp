package com.example.feature_drinks.presentation.view.fragment.drinkDetailsFragment

import com.example.feature_drinks.data.remote.response.DrinkDetailDTO

data class DrinkDetailsState(
    val details: List<DrinkDetailDTO.Drink> = emptyList()
)
