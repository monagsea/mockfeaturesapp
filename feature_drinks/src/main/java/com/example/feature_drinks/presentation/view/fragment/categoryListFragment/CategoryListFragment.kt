package com.example.feature_drinks.presentation.view.fragment.categoryListFragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.feature_drinks.data.repo.remoteModule
import com.example.feature_drinks.databinding.FragmentCategoryListBinding
import com.example.feature_drinks.di.*
import com.example.feature_drinks.presentation.view.adapter.CategoryAdapter
import com.example.feature_drinks.presentation.viewmodel.CategoryListViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.context.loadKoinModules

class CategoryListFragment : Fragment() {

    private var _binding: FragmentCategoryListBinding? = null
    private val binding get() = _binding!!
    private val categoryViewModel by viewModel<CategoryListViewModel>()
    private val categoryAdapter by lazy { CategoryAdapter() }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentCategoryListBinding.inflate(
        inflater, container, false
    ).also { _binding = it }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        loadKoinModules(
            modules = listOf(
                categoryViewModelModule,
                drinkListViewModelModule,
                drinkDetailViewModel,
                localModule,
                remoteModule
            )
        )



        with(binding) {
            rvCategories.layoutManager = LinearLayoutManager(context)
            rvCategories.adapter = categoryAdapter.apply {
                with(categoryViewModel) {
                    browseCategories()
                    categoryList.observe(viewLifecycleOwner) {
                        addCategories(it)
                    }
                }
            }
        }
    }
}