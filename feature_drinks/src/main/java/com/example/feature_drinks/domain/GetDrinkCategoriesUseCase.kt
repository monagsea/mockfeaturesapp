package com.example.feature_drinks.domain

import com.example.feature_drinks.data.repo.DrinksRepo
import com.example.local.drinksLocal.entity.Category
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class GetDrinkCategoriesUseCase(
    private val repo: DrinksRepo
) {
    suspend operator fun invoke(): List<Category> = withContext(Dispatchers.IO) {
        repo.getDrinkCategories()
    }
}
