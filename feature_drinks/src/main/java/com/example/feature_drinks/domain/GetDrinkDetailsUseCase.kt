package com.example.feature_drinks.domain

import com.example.feature_drinks.data.remote.response.DrinkDetailDTO
import com.example.feature_drinks.data.repo.DrinksRepo
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class GetDrinkDetailsUseCase(
    private val repo: DrinksRepo
) {
    suspend operator fun invoke(id: String): DrinkDetailDTO = withContext(Dispatchers.IO) {
        repo.getDrinkDetailsById(id)
    }
}
