package com.example.feature_drinks.domain

import com.example.feature_drinks.data.remote.response.CategoryDrinksDTO
import com.example.feature_drinks.data.repo.DrinksRepo
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class GetDrinkListByIdUseCase(
    private val repo: DrinksRepo
) {

    suspend operator fun invoke(category: String): CategoryDrinksDTO = withContext(Dispatchers.IO) {
        repo.getDrinksByCategory(category)
    }

}
