package com.example.feature_drinks.di

import com.example.di.provideDatabase
import com.example.feature_drinks.data.repo.DrinksRepo
import com.example.feature_drinks.domain.GetDrinkCategoriesUseCase
import com.example.feature_drinks.domain.GetDrinkDetailsUseCase
import com.example.feature_drinks.domain.GetDrinkListByIdUseCase
import com.example.feature_drinks.presentation.viewmodel.CategoryListViewModel
import com.example.feature_drinks.presentation.viewmodel.DrinkDetailViewModel
import com.example.feature_drinks.presentation.viewmodel.DrinkListViewModel

import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

private const val BASE_URL = "https://thecocktaildb.com"

val categoryViewModelModule = module {
    viewModel {
        CategoryListViewModel(
            getDrinkCategoriesUseCase = GetDrinkCategoriesUseCase(
                repo = DrinksRepo(
                    service = provideRetrofit(BASE_URL),
                    database = provideDatabase(androidContext())
                )
            )
        )
    }
}

val drinkListViewModelModule = module {
    viewModel {
        DrinkListViewModel(
            getDrinkListByIdUseCase = GetDrinkListByIdUseCase(
                repo = DrinksRepo(
                    service = provideRetrofit(BASE_URL),
                    database = provideDatabase(androidContext())

                )
            )
        )
    }
}

val drinkDetailViewModel = module {
    viewModel {
        DrinkDetailViewModel(
            getDrinkDetailsUseCase = GetDrinkDetailsUseCase(
                repo = DrinksRepo(
                    service = provideRetrofit(BASE_URL),
                    database = provideDatabase(androidContext())
                )
            )
        )
    }
}
