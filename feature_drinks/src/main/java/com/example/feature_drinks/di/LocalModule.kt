package com.example.feature_drinks.di

import com.example.di.provideRetrofitBuilder
import com.example.feature_drinks.data.remote.DrinksService
import org.koin.dsl.module

val localModule = module {
    single { provideRetrofit("https://thecocktaildb.com") }
}

fun provideRetrofit(baseUrl: String): DrinksService {
    return provideRetrofitBuilder(baseUrl).create(DrinksService::class.java)

}
