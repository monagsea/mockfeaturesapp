package com.example.mockfeaturesapp

import android.app.Application
import com.example.di.databaseModule
import com.example.di.retrofitBuilder
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.GlobalContext.startKoin

class MainApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@MainApplication)
            androidLogger()
            modules(
                retrofitBuilder,
                databaseModule
            )
        }
    }
}