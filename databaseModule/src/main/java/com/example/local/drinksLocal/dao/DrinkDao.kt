package com.example.local.drinksLocal.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.local.drinksLocal.entity.Drink

@Dao
interface DrinkDao {
    @Query("SELECT * FROM Drink WHERE id = :id")
    suspend fun getDrink(id: String): Drink

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertDrink(drink: Drink)
}