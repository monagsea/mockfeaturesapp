package com.example.local.drinksLocal.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.local.drinksLocal.entity.DrinkList

@Dao
interface DrinkListDao {
    @Query("SELECT * FROM Drinks")
    suspend fun getCategories(): List<DrinkList>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertCategory(drinkList: DrinkList)
}