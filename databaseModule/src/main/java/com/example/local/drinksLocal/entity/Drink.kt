package com.example.local.drinksLocal.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "Drink")
data class Drink(
    @PrimaryKey(autoGenerate = true)
    val id: Int = 0,
    val strDrink: String,
    val strDrinkThumb: String,
    val ingredient1: String,
    val ingredient2: String,
    val ingredient3: String
)
