package com.example.local.drinksLocal.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "Drinks")
data class DrinkList(
    @PrimaryKey()
    val idDrink: String,
    val strDrink: String,
    val strDrinkThumb: String
)
