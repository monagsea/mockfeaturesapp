package com.example.local.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.local.drinksLocal.dao.CategoryDao
import com.example.local.drinksLocal.dao.DrinkDao
import com.example.local.drinksLocal.dao.DrinkListDao
import com.example.local.drinksLocal.entity.Category
import com.example.local.drinksLocal.entity.Drink
import com.example.local.drinksLocal.entity.DrinkList
import com.example.local.shibeLocal.dao.ShibeDao
import com.example.local.shibeLocal.entity.Shibe

@Database(entities = [Category::class, Drink::class, DrinkList::class, Shibe::class], version = 2)
abstract class MainDatabase : RoomDatabase() {

    abstract fun categoryDao(): CategoryDao
    abstract fun userDao(): DrinkListDao
    abstract fun drinkDao(): DrinkDao
    abstract fun shibeDao(): ShibeDao

}