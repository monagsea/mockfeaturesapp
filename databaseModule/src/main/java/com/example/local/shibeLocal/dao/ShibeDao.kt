package com.example.local.shibeLocal.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.example.local.shibeLocal.entity.Shibe

@Dao
interface ShibeDao {

    @Query("SELECT * FROM shibe")
    suspend fun getAll(): List<Shibe>

    @Insert
    suspend fun insert(shibe: List<Shibe>)

}