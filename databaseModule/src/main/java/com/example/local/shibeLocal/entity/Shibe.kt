package com.example.local.shibeLocal.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Shibe(
    @PrimaryKey(autoGenerate = true) val id: Int = 0,
    val url: String
)