package com.example.di

import android.content.Context
import androidx.room.Room
import com.example.local.database.MainDatabase
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module


val databaseModule = module {
    single { provideDatabase(this.androidContext()) }
}

private const val DB_NAME = "main.db"

@Volatile
private var instance: MainDatabase? = null

private fun databaseBuilder(context: Context): MainDatabase = Room.databaseBuilder(
    context, MainDatabase::class.java, DB_NAME
).build()

fun provideDatabase(context: Context): MainDatabase {

    return instance ?: databaseBuilder(context).also {
        instance = it
    }
}