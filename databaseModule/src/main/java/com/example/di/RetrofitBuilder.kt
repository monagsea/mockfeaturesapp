package com.example.di

import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

val retrofitBuilder = module {
    single { provideRetrofitBuilder(get()) }
}

fun provideRetrofitBuilder(baseUrl: String): Retrofit {

    return Retrofit.Builder()
        .baseUrl(baseUrl)
        .addConverterFactory(GsonConverterFactory.create())
        .build()
}
