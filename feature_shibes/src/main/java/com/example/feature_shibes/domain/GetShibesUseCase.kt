package com.example.feature_shibes.domain

import android.util.Log
import com.example.feature_shibes.data.repo.ShibeRepo
import com.example.local.shibeLocal.entity.Shibe
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class GetShibesUseCase(
    private val repo: ShibeRepo
) {
    val TAG = "GetShibesUseCase"
    suspend operator fun invoke(): List<Shibe> = withContext(Dispatchers.IO) {
        Log.e(TAG, "invoke: getting shibes from USecase")
        repo.getShibes()
    }
}
