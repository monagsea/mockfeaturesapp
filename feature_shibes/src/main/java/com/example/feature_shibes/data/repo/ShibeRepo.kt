package com.example.feature_shibes.data.repo

import com.example.di.provideDatabase
import com.example.feature_shibes.data.remote.ShibeService
import com.example.feature_shibes.data.remote.response.ShibeDTO
import com.example.feature_shibes.di.provideRetrofit
import com.example.local.database.MainDatabase
import com.example.local.shibeLocal.dao.ShibeDao
import com.example.local.shibeLocal.entity.Shibe
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val shibeRepoModule = module {
    single { ShibeRepo(provideRetrofit("https://shibe.online"), provideDatabase(androidContext())) }
}

class ShibeRepo(
    private val service: ShibeService, shibeDatabase: MainDatabase
) {
    val TAG = "ShibeRepo"
    val shibeDao: ShibeDao = shibeDatabase.shibeDao()

    suspend fun getShibes() = withContext(Dispatchers.IO) {
        val cachedShibes: List<Shibe> = shibeDao.getAll()
        return@withContext cachedShibes.ifEmpty {
            val shibeUrls: List<String> = service.getShibes()
            val shibes: List<Shibe> = shibeUrls.map {
                Shibe(
                    url = it
                )
            }
            shibeDao.insert(shibes)
            return@ifEmpty shibes
        }
    }
}