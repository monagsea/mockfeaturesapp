package com.example.feature_shibes.data.remote.response


data class ShibeDTO(
    val shibes: List<Shibe>
) {
    data class Shibe(
        val url: String
    )
}