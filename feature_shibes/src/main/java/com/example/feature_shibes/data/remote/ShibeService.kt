package com.example.feature_shibes.data.remote

import retrofit2.http.GET
import retrofit2.http.Query

interface ShibeService {

    @GET("/api/shibes")
    suspend fun getShibes(@Query("count") count: Int = 100): List<String>

}