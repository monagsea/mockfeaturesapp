package com.example.feature_shibes.presentation.view.fragment

import com.example.local.shibeLocal.entity.Shibe


data class ShibeState(
    val shibe: List<Shibe> = emptyList()
)