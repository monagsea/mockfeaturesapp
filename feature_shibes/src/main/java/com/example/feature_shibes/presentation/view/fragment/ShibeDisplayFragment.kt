package com.example.feature_shibes.presentation.view.fragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.feature_shibes.data.repo.shibeRepoModule
import com.example.feature_shibes.databinding.FragmentShibeDisplayBinding
import com.example.feature_shibes.di.shibeLocalModule
import com.example.feature_shibes.di.shibeViewModelModule
import com.example.feature_shibes.presentation.view.adapter.ShibeAdapter
import com.example.feature_shibes.presentation.viewmodel.ShibeViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.context.loadKoinModules


class ShibeDisplayFragment : Fragment() {
    private val shibeViewModel by viewModel<ShibeViewModel>()
    private val shibeAdapter by lazy { ShibeAdapter() }
    private var _binding: FragmentShibeDisplayBinding? = null
    private val binding: FragmentShibeDisplayBinding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentShibeDisplayBinding.inflate(
        inflater, container, false
    ).also { _binding = it }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        loadKoinModules(
            modules = listOf(
                shibeLocalModule,
                shibeViewModelModule,
                shibeRepoModule
            )
        )

        with(binding) {
            rvShibes.layoutManager = LinearLayoutManager(context)
            rvShibes.adapter = shibeAdapter.apply {
                with(shibeViewModel) {
                    browseShibes()
                    Log.i("TEST3", "onViewCreated: TEST3")
                    shibes.observe(viewLifecycleOwner) {
                        Log.i("TEST1", "onViewCreated: $it")
                        addShibes(it)

                    }
                }
            }
        }
    }
}