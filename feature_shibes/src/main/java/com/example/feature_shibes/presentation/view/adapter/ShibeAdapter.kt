package com.example.feature_shibes.presentation.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.feature_shibes.databinding.ItemShibeBinding
import com.example.local.shibeLocal.entity.Shibe
import com.squareup.picasso.Picasso

class ShibeAdapter() : RecyclerView.Adapter<ShibeAdapter.ShibeViewHolder>() {

    private var shibes = listOf<Shibe>()

    override fun onCreateViewHolder(
        parent: ViewGroup, viewType: Int
    ) = ShibeViewHolder.getInstance(parent)

    override fun onBindViewHolder(holder: ShibeViewHolder, position: Int) {
        val url = shibes[position].url
        holder.loadShibeImage(url)
    }

    fun addShibes(shibes: List<Shibe>) {
        this.shibes = shibes
        notifyDataSetChanged()
    }

    override fun getItemCount() = shibes.size

    class ShibeViewHolder(
        private val binding: ItemShibeBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun loadShibeImage(url: String) {
            with(binding) {
                Picasso.get().load(url).into(ivShibe)
            }
        }

        companion object {
            fun getInstance(parent: ViewGroup) = ItemShibeBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            ).let { binding -> ShibeViewHolder(binding) }
        }
    }
}
