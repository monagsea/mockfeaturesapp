package com.example.feature_shibes.presentation.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.feature_shibes.domain.GetShibesUseCase
import com.example.feature_shibes.presentation.view.fragment.ShibeState
import com.example.local.shibeLocal.entity.Shibe
import kotlinx.coroutines.launch

class ShibeViewModel(
    private val getShibesUseCase: GetShibesUseCase
) : ViewModel() {

    private val _shibesState = MutableLiveData(ShibeState())
    val shibesState: LiveData<ShibeState> get() = _shibesState

    private val _shibeList = MutableLiveData<List<Shibe>>()
    val shibes: LiveData<List<Shibe>> get() = _shibeList

    fun browseShibes() {
        viewModelScope.launch {
            Log.i("TEST5", "browseShibes")
            _shibesState.value = ShibeState(shibe = getShibesUseCase.invoke())
            _shibeList.value = getShibesUseCase.invoke()
            Log.i("TEST4", "browseShibes: $shibes")
        }
    }

}