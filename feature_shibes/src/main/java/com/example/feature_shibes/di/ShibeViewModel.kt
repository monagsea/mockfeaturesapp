package com.example.feature_shibes.di

import com.example.di.provideDatabase
import com.example.feature_shibes.data.repo.ShibeRepo
import com.example.feature_shibes.domain.GetShibesUseCase
import com.example.feature_shibes.presentation.viewmodel.ShibeViewModel
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val shibeViewModelModule = module {
    viewModel {
        ShibeViewModel(
            getShibesUseCase = GetShibesUseCase(
                repo = ShibeRepo(
                    provideRetrofit(
                        baseUrl = "https://shibe.online"),
                    provideDatabase(
                        androidContext()
                    )
                )
            )
        )
    }
}

