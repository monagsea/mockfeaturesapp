package com.example.feature_shibes.di

import com.example.di.provideRetrofitBuilder
import com.example.feature_shibes.data.remote.ShibeService
import org.koin.dsl.module

val shibeLocalModule = module {
    single { provideRetrofit("https://shibe.online") }
}

fun provideRetrofit(baseUrl: String): ShibeService {
    return provideRetrofitBuilder(baseUrl).create(ShibeService::class.java)
}
